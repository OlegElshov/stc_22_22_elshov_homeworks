package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.LessonForm;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.repositories.CoursesRepository;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.services.LessonsService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class LessonsServiceImpl implements LessonsService {
    private final LessonsRepository lessonsRepository;

    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAllByStateNot(Lesson.State.DELETED);
    }

    @Override
    public Lesson getLesson(Long lessonId) {
        return lessonsRepository.findById(lessonId).orElseThrow();
    }

    @Override
    public void addLesson(LessonForm lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .startTime(lesson.getStartTime())
                .finishTime(lesson.getFinishTime())
                .state(Lesson.State.CONFIRMED)
                .summary(lesson.getSummary())
                .build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lessonForDelete = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForDelete.setState(Lesson.State.DELETED);

        lessonsRepository.save(lessonForDelete);
    }

    @Override
    public void updateLesson(Long lessonId, LessonForm updateData) {
        Lesson lessonForUpdate = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForUpdate.setStartTime(updateData.getStartTime());
        lessonForUpdate.setFinishTime(updateData.getFinishTime());
        lessonForUpdate.setSummary(updateData.getSummary());
        lessonsRepository.save(lessonForUpdate);
    }
}
