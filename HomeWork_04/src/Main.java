import java.util.Scanner;

public class Main {
    public static int culcSumOfArrayRange(int from, int to) {

        int sum = 0;

        if (from < to) {
            for (int i = from; i <= to; i++) {
                sum += i;
            }
        } else {
            sum = -1;
        }
        return sum;
    }
    public static void isEven(int[] a) {

        int count = 0;

        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                int even = a[i];
                count += 1;
                System.out.println(even);
            }
        }
        if (count == 0) {
            System.out.println("without even");
        }
    }
        public static void main(String[] args) {

            Scanner scanner = new Scanner(System.in);

            System.out.println("Enter 'from'");

            int from1 = scanner.nextInt();

            System.out.println("Enter 'to'");

            int to1 = scanner.nextInt();

            int sum1 = culcSumOfArrayRange(from1, to1);

            System.out.println("sum = " + sum1);

            System.out.println("Enter length");

            int length = scanner.nextInt();

            System.out.println("Enter array");

            int[] array = new int[length];

            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
            }
            isEven(array);
        }
}
