public class Main {
    public static void main(String[] args) {

        ArrayTask sumRangeFromTo = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum = sum + array[i];
            }
            return sum;
        };
        ArrayTask sumNumberMaxInRange = (array, from, to) -> {
            int max = array[from];
            int sumNumber = 0;
            for (int i = from; i < to; i++) {
                if (max < array[i + 1]) {
                    max = array[i + 1];
                }
            }
            while (max != 0) {
                sumNumber += max % 10;
                max = max / 10;
            }
            return sumNumber;
        };
        int[] array1 = {4, 7, 84, 155, 22};
        ArrayTaskResolver.resolveTask(array1, sumRangeFromTo, 1, 2);
        ArrayTaskResolver.resolveTask(array1, sumNumberMaxInRange, 1, 3);
    }
}