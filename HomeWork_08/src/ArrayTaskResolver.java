import java.util.Arrays;

public class ArrayTaskResolver {
    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println(Arrays.toString(array));
        System.out.println("from: " + from + " | to: " + to);
        System.out.println(task.resolve(array, from, to));
    }
}
