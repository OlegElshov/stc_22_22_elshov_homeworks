public class Ellipse extends Circle {

    private double r2;

    public Ellipse(double x, double y, double r, double r2) {
        super(x, y, r);
        this.r2 = r2;
    }

    public double getR2() {
        return r2;
    }

    public double area() {
        double S = r * r2 * Math.PI;
        return S;
    }

    public double perimeter() {
        double P = 4 * (((Math.PI * r * r2) + (Math.pow((r2 - r), 2))) / (r + r2));
        return P;
    }
}
