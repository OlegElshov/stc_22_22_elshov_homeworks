public class Circle extends Shape {

    protected double r;

    public Circle(double x, double y, double r) {
        super(x, y);
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public double area() {
        double S = r * r * Math.PI;
        return S;
    }

    public double perimeter() {
        double P = 2 * Math.PI * r;
        return P;
    }
}
