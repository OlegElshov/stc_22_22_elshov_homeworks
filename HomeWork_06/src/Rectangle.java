public class Rectangle extends Square {

    private double b;

    public Rectangle(double x, double y, double a, double b) {

        super(x, y, a);
        this.b = b;
    }

    public double getB() {
        return b;
    }

    public double area() {
        double S = a * b;
        return S;
    }

    public double perimeter() {
        double P = 2 * (a + b);
        return P;
    }
}

