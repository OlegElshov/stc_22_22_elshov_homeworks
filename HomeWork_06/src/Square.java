public class Square extends Shape {
    double a;

    public Square(double x, double y, double a) {

        super(x, y);
        this.a = a;
    }

    public double getA() {
        return a;
    }

    public double area() {
        double S = a * a;
        return S;
    }

    public double perimeter() {
        double P = 2 * (a + a);
        return P;
    }
}
