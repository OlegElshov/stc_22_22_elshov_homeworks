public class Main {

    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(5, 10,5,7);
        Square square = new Square(8, 5,3);
        Circle circle = new Circle(7, 9, 8);
        Ellipse ellipse = new Ellipse(8, 14, 5, 9);

        rectangle.moveTo(-1,-1);
        System.out.println("P = " + rectangle.perimeter() + " S = " + rectangle.area() + " x = " + rectangle.getX() + " y = " + rectangle.getY());
        System.out.println("P = " + square.perimeter() + " S = " + square.area() + " x = " + square.getX() + " y = " + square.getY());
        System.out.println("P = " + ellipse.perimeter() + " S = " + ellipse.area() + " x = " + ellipse.getX() + " y = " + ellipse.getY());
        System.out.println("P = " + circle.perimeter() + " S = " + circle.area() + " x = " + circle.getX() + " y = " + circle.getY());
    }

}