public class CarsService {
    private CarsRepository carsRepository;

    public CarsService(CarsRepository carsRepository) {
        this.carsRepository = carsRepository;
    }

    public int counOfAllCars() {
        return carsRepository.findAll().size();
    }
}
