import java. util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int length = scanner.nextInt();
        int count = 0;
        int[] array = new int[length];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        if (array[0] < array[1]) {
            count += 1;
        }

        for (int i = 1; i < array.length - 1; i++) {
            if (array[i] < array[i + 1] && array[i] < array[i-1] ) {
                count += 1;
                }
            }
        if (array[array.length - 1] < array[array.length - 2]) {
            count += 1;
        }
        System.out.println(count);
    }
}