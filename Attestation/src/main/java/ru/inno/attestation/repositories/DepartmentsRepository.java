package ru.inno.attestation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.attestation.models.Department;

import java.util.List;

public interface DepartmentsRepository extends JpaRepository<Department, Long> {
    List<Department> findAllByStateNot(Department.State state);
}
