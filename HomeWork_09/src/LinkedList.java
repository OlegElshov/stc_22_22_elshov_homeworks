public class LinkedList<T> implements List<T> {

    private Node<T> first;
    private Node<T> last;

    private int count;

    private class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }

    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public void remove(T element) {
        if (contains(element)) {
            Node<T> current = this.first;
            int counter = 0;
            while (element.equals(current.value) == false) {
                counter++;
                current = current.next;
            }
            removeAt(counter);
        }
    }

    @Override
    public boolean contains(T element) {
        Node<T> current = this.first;
        while (current != null) {
            if (current.value.equals(element)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        if (index >= 0 && index < count) {
            Node<T> current = this.first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            for (int i = index; i < size() - 1; i++) {
                current.value = current.next.value;
                current = current.next;
            }
            current.next = null;
            count--;
        }
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            Node<T> current = this.first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        return null;
    }
}

