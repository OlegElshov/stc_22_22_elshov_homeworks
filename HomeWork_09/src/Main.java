import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        List<Integer> integerList = new LinkedList<>();
        integerList.add(8);
        integerList.add(10);
        integerList.add(11);
        integerList.add(13);
        integerList.add(11);
        integerList.add(15);
        integerList.add(-5);

        System.out.println(integerList.get(0));
        System.out.println(integerList.size());
        System.out.println(integerList.contains(15));

        integerList.remove(8);
        integerList.removeAt(2);

        for (int i = 0; i < integerList.size(); i++) {
            System.out.print(integerList.get(i) + " ");

        }
    }
}
