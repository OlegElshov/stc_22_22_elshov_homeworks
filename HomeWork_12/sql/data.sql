insert into client (first_name, last_name, tel_number, experience, age, driver_license, driver_license_category, rating)
values ('Oleg', 'Elshov', 79999999999, 5, 30, true, 'B', 5),
       ('Александр', 'Владимиров', 78888888888, 6, 31, true, 'A', 4),
       ('Василий', 'Васильев', 77777777777, 7, 32, true, 'C', 4),
       ('Федор', 'Федоров', 76666666666, 8, 33, false, 'B', 5),
       ('Николай', 'Николаев', 75555555555, 9, 33, true, 'B', 3)
;
insert into car (car_model, car_number, car_color, car_id)
values ('Camry', 'A777AA', 'Black', 1),
       ('Lada', 'A555AA', 'Yelow', 2),
       ('BMV', 'A333AA', 'Green', 3),
       ('Jeep', 'A888AA', 'Grey', 4),
       ('Toyota', 'A999AA', 'White', 5)
;
insert into trip (driver_id, car_id, trip_date, trip_time)
values (1, 2, '2022-01-10', '01:30:45'),
       (1, 2, '2022-02-10', '00:30:45'),
       (2, 4, '2022-03-10', '02:30:45'),
       (3, 5, '2022-04-10', '01:34:45'),
       (4, 4, '2022-05-10', '01:23:45')
;