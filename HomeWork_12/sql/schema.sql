drop table if exists client;
drop table if exists car;
drop table if exists trip;
create table client
(
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    tel_number varchar(20),
    experience integer,
    age integer check ( age >= 0 and age <= 120 ),
    driver_license bool,
    driver_license_category char(20),
    rating integer check ( rating >= 0 and rating <= 5 )
);
create table car
(
    id bigserial primary key,
    car_model char(20) not null ,
    car_number char(20) not null ,
    car_color char(20) not null,
    car_id integer,
    foreign key (car_id) references client (id)
);
create table trip
(
    driver_id bigint,
    car_id bigint,
    foreign key (driver_id) references client(id),
    foreign key (car_id) references car(id),
    trip_date timestamp,
    trip_time time
);
