import java.util.Scanner;
public class Main {
    public static int toInt(int[] a) {

        int number = 0;

        for (int i = 0; i < a.length; i++) {
        number += a[i] * Math.pow(10, a.length - i - 1);
        }
        return number;
    }
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter length");

        int length = scanner.nextInt();
        int[] array = new int[length];

        System.out.println("Enter array");

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int result = toInt(array);

        System.out.println(result);
    }
}
