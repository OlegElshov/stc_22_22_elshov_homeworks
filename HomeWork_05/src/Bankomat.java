public class Bankomat {

    public static final int MAX_VOLUME_MONEY = 100000;
    public static final int MAX_SUM_RAZRESH = 7000;
    private int remainingMoney;
    private int countOperations;

    public int getRemainingMoney() {
        return remainingMoney;
    }

    public int getCountOperations() {
        return countOperations;
    }

    public Bankomat() {
        this.remainingMoney = MAX_VOLUME_MONEY;
        this.countOperations = 0;
    }

    public int giveMoney(int sumMoney) {
        this.countOperations++;
        if (sumMoney <= this.remainingMoney && sumMoney <= MAX_SUM_RAZRESH) {
            this.remainingMoney -= sumMoney;
            return sumMoney;
        } else {
            return 0;
        }
    }

    public int putMoney(int sumMoney) {
        this.countOperations++;
        if (sumMoney > (MAX_VOLUME_MONEY - this.remainingMoney)) {
            sumMoney = sumMoney - (MAX_VOLUME_MONEY - this.remainingMoney);
            this.remainingMoney = MAX_VOLUME_MONEY;
            return sumMoney;
        } else {
            this.remainingMoney += sumMoney;
            return 0;
        }
    }
}


