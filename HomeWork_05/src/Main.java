import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Bankomat ATM = new Bankomat();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Максимальная сумма в банкомате - " + ATM.MAX_VOLUME_MONEY);
        System.out.println("Сумма оставшихся денег в банкомате - " + ATM.getRemainingMoney());
        System.out.println("Максимальная сумма, рарешенная к выдаче - " + ATM.MAX_SUM_RAZRESH);
        System.out.println("Количество проведенных операций - " + ATM.getCountOperations());
        System.out.println("==================================================");
        System.out.println("Снять деньги - 1, положить деньги - 2, выйти из программы - 0");

        int deystvie = scanner.nextInt();
        int ostatok = 0;

        while (deystvie != 0) {
            if (deystvie == 1) {
                System.out.println("Введите сумму для снятия");
                int vyvod = scanner.nextInt();
                ATM.giveMoney(vyvod);
            }
            if (deystvie == 2) {
                System.out.println("Введите сумму для внесения");
                int vvod = scanner.nextInt();
                ostatok = ATM.putMoney(vvod);
            }
            System.out.println("Максимальная сумма в банкомате - " + ATM.MAX_VOLUME_MONEY);
            System.out.println("Сумма оставшихся денег в банкомате - " + ATM.getRemainingMoney());
            System.out.println("Максимальная сумма, рарешенная к выдаче - " + ATM.MAX_SUM_RAZRESH);
            System.out.println("Количество проведенных операций - " + ATM.getCountOperations());
            if (ostatok > 0) {
                System.out.println("Превышен лимит (заберите остаток) - " + ostatok);
            }
            System.out.println("==================================================");
            System.out.println("Снять деньги - 1, положить деньги - 2, выйти из программы - 0");
            deystvie = scanner.nextInt();
        }
    }
}
